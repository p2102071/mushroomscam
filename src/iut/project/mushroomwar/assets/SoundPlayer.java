/*
 * Copyright (C) 2022 IUT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package iut.project.mushroomwar.assets;



/**
 * Cette classe permet de jouer des sons. Les sons disponibles sont: 
 * 
 * <br>
 * <ul>
 * <li>{@link #ambiance()}  &rarr;  (musique d'ambiance) qui doit s'exécuter en boucle jusqu'à l'appel de la méthode {@link #stopAmbiance()}</li>
 * <li>{@link #sound(iut.project.mushroomwar.assets.Sound)}  &rarr;  qui exécute un {@link Sound} en particulier (il s'agit forcément de sons brefs)</li>
 * <li>{@link #youLost()}  &rarr;  qui doit s'exécuter lorsque le joueur humain a perdu (le son met temporairement la musique d'ambiance en pause le temps de jouer celui-ci)</li>
 * <li>{@link #youWon()}  &rarr;  qui doit s'exécuter lorsque le joueur humain a gagné (le son met temporairement la musique d'ambiance en pause le temps de jouer celui-ci)</li>
 * </ul>
 * @author Joseph BRIGUET
 * @version 1.0
 */
public final class SoundPlayer {
    
    
    
//ATTRIBUTS STATICS
    /**
     * Correspond au player de la musique d'ambiance
     */
    private static javax.sound.sampled.Clip ambiance;
    
    /**
     * Correspond au controleur de volume du player de la musique d'ambiance
     */
    private static javax.sound.sampled.FloatControl gainControl;
    
    /**
     * Correspond à l'intervalle possible du controleur de volume du player de la musique d'ambiance
     */
    private static float range;

    
    
//CONSTRUCTOR
    /**
     * Correspond au constructeur par défaut
     * @deprecated NE PAS UTILISER
     */
    @Deprecated
    private SoundPlayer() {}
    
    
    
//METHODES PUBLICS STATICS
    /**
     * Joue la musique d'ambiance
     */
    @SuppressWarnings("UseSpecificCatch")
    public final static void ambiance(){
        try {
            ambiance = javax.sound.sampled.AudioSystem.getClip();
            javax.sound.sampled.AudioInputStream inputStream = javax.sound.sampled.AudioSystem.getAudioInputStream(SoundPlayer.class.getResource("ambiance.wav"));
            ambiance.open(inputStream);
            ambiance.loop(javax.sound.sampled.Clip.LOOP_CONTINUOUSLY);
            gainControl = (javax.sound.sampled.FloatControl) ambiance.getControl(javax.sound.sampled.FloatControl.Type.MASTER_GAIN);
            range = gainControl.getMaximum() - gainControl.getMinimum();
            float gain = (range * 0.75f) + gainControl.getMinimum();
            gainControl.setValue(gain);
            ambiance.start();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(SoundPlayer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Joue un son bref
     * @see Sound Contient la liste de tous les sons brefs possibles
     * @param sound Correspond au son à jouer
     */
    @SuppressWarnings("UseSpecificCatch")
    public final static void sound(Sound sound){
        if (sound != null) {
            try {
                javax.sound.sampled.Clip clip = javax.sound.sampled.AudioSystem.getClip();
                javax.sound.sampled.AudioInputStream inputStream = javax.sound.sampled.AudioSystem.getAudioInputStream(SoundPlayer.class.getResource(sound.toString()));
                clip.open(inputStream);
                javax.sound.sampled.FloatControl fc = (javax.sound.sampled.FloatControl) clip.getControl(javax.sound.sampled.FloatControl.Type.MASTER_GAIN);
                float r = fc.getMaximum() - fc.getMinimum();
                float g = (r * sound.volume()) + fc.getMinimum();
                fc.setValue(g);
                clip.start();
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(SoundPlayer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        } else {
            throw new NullPointerException("Sound is null !");
        }
    }
    
    /**
     * Stoppe la musique d'ambiance
     */
    @SuppressWarnings("SleepWhileInLoop")
    public final static void stopAmbiance(){
        if(ambiance.isRunning()){
            float gain = (range * 0.75f) + gainControl.getMinimum();
            new Thread(() -> {
                float v = 0.75f;
                while(v > 0.0f){
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException ex) {
                        java.util.logging.Logger.getLogger(SoundPlayer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                    }
                    v -= 0.01f;
                    if(v>0.0f)
                        gainControl.setValue(gain);
                }
                ambiance.stop();
                ambiance.close();
            }).start();
        }
    }
    
    /**
     * Joue le son du joueur humain perdant. Cette méthode met temporairement la musique d'ambiance en pause
     */
    public final static void youLost(){
        stopAmbiance();
        sound(Sound.YOU_LOST);
        new Thread(() -> {
            try {
                Thread.sleep(6000);
            } catch (InterruptedException ex) {
                java.util.logging.Logger.getLogger(SoundPlayer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            ambiance();
        }).start();
    }
    
    /**
     * Joue le son du joueur humain gagnant. Cette méthode met temporairement la musique d'ambiance en pause
     */
    public final static void youWon(){
        stopAmbiance();
        sound(Sound.YOU_WON);
        new Thread(() -> {
            try {
                Thread.sleep(3500);
            } catch (InterruptedException ex) {
                java.util.logging.Logger.getLogger(SoundPlayer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            ambiance();
        }).start();
    }
    
    
    
}