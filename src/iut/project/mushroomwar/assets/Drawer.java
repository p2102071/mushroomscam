/*
 * Copyright (C) 2022 IUT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package iut.project.mushroomwar.assets;



/**
 * Cette classe regroupe quelques méthodes de classe statique, permettant sur un graphique:
 * 
 * <br>
 * <ul>
 * <li>{@link #paintCounterdown(java.awt.Graphics, int, int, int, java.awt.Font, java.awt.Color)}  &rarr;  de dessiner le compte à rebours avant de commencer la partie</li>
 * <li>{@link #paintLabelMushroom(java.awt.Graphics, int, int, int)}  &rarr;  de dessiner une étiquette (pour un champignon) avec le nombre d'unités à l'intérieur du champignon</li>
 * <li>{@link #paintMushroom(java.awt.Graphics, int, int, java.awt.Color)}  &rarr;  de dessiner un champignon d'une couleur à une coordonnée [x;y] donnée</li>
 * <li>{@link #paintUnit(java.awt.Graphics, int, int, int, java.awt.Color)}  &rarr;  de dessiner des unités d'attaques (en déplacement) de la couleur de l'attaquant à une coordonnée [x;y] donnée</li>
 * </ul>
 * @author Joseph BRIGUET
 * @version 1.0
 */
public final class Drawer {

    
    
//CONSTRUCTOR
    /**
     * Correspond au constructeur par défaut
     * @deprecated NE PAS UTILISER
     */
    @Deprecated
    private Drawer() {}
    
    
    
//METHODES PUBLICS STATICS
    /**
     * Renvoie l'icône de l'application
     * @return Retourne l'icône de l'application
     */
    public final static java.awt.Image getAppIcon() {
        return java.awt.Toolkit.getDefaultToolkit().createImage(Drawer.class.getResource("logo.png"));
    }
    
    /**
     * Renvoie l'image d'arrière plan de la map de jeu
     * @return Retourne l'image d'arrière plan de la map de jeu
     */
    public final static java.awt.Image getBackgroundMap() {
        try {
            return javax.imageio.ImageIO.read(Drawer.class.getResource("map.jpg"));
        } catch (java.io.IOException ex) {
            java.util.logging.Logger.getLogger(Drawer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            return null;
        }
    }
    
    /**
     * Renvoie l'image lorsque vous avez perdu
     * @return Retourne l'image lorsque vous avez perdu
     */
    public final static java.awt.Image getYouLostImage() {
        try {
            return javax.imageio.ImageIO.read(Drawer.class.getResource("you_lost.jpg"));
        } catch (java.io.IOException ex) {
            java.util.logging.Logger.getLogger(Drawer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            return null;
        }
    }
    
    /**
     * Renvoie l'image lorsque vous avez gagné
     * @return Retourne l'image lorsque vous avez gagné
     */
    public final static java.awt.Image getYouWonImage() {
        try {
            return javax.imageio.ImageIO.read(Drawer.class.getResource("you_won.jpg"));
        } catch (java.io.IOException ex) {
            java.util.logging.Logger.getLogger(Drawer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            return null;
        }
    }
    
    /**
     * Renvoie les dimensions d'un texte par rapport à une police de caractères
     * @param g Correspond au graphique qui sert de référence pour calculer les dimensions du texte
     * @param font Correspond à la police de caractères
     * @param text Correspond au texte dont on cherche à déterminer les dimensions
     * @return Retourne les dimensions du texte
     */
    public final static java.awt.Dimension getDimensionText(java.awt.Graphics g, java.awt.Font font, String text) {
        java.awt.FontMetrics metrics = g.getFontMetrics(font);
        return new java.awt.Dimension(metrics.stringWidth(text), metrics.getHeight());
    }
    
    /**
     * Peint le compte à rebours sur un objet {@link java.awt.Graphics}
     * @param g Correspond au graphique (ou toile) de l'objet à repeindre
     * @param width Correspond à la largeur de l'objet à repeindre (la plupart du temps, c'est de la map qu'il s'agit)
     * @param height Correspond à la hauteur de l'objet à repeindre (la plupart du temps, c'est de la map qu'il s'agit)
     * @param value Correspond à la valeur du compte à rebours
     * @param font Correspond à la police de caractère du compte à rebours
     * @param color Correspond à la couleur du compte à rebours
     */
    public final static void paintCounterdown(java.awt.Graphics g, int width, int height, int value, java.awt.Font font, java.awt.Color color) {
        int width2 = width / 2;
        int height2 = height / 2;
        
        java.awt.FontMetrics metrics = g.getFontMetrics(font);
        int w = metrics.stringWidth(""+value);
        int h = metrics.getHeight();
        int w2 = w / 2;
        int h2 = h / 2;
        
        g.setColor(color);
        g.setFont(font);
        g.drawString(""+value, width2 - w2, height2 + h2);
    }
    
    /**
     * Peint le nombre d'unités d'un champignon sur un objet {@link java.awt.Graphics}
     * @param g Correspond au graphique (ou toile) de l'objet à repeindre
     * @param x Correspond au point sur l'axe des abscisses où est peint le champignon associé à cette étiquette
     * @param y Correspond au point sur l'axe des ordonnées où est peint le champignon associé à cette étiquette
     * @param countUnits Correspond au nombre d'unités à afficher sur le label du champignon
     */
    public final static void paintLabelMushroom(java.awt.Graphics g, int x, int y, int countUnits) {
        if(countUnits > 0){
            int xM = x + 17;
            int yM = y + 64 + 6;
            int wM = 30;
            int hM = 15;
            String text = "" + countUnits;
            
            g.setColor(java.awt.Color.WHITE);
            g.fillRect(xM, yM, wM, hM);
            g.setColor(java.awt.Color.BLACK);
            g.drawRect(xM, yM, wM, hM);
            java.awt.FontMetrics metrics = g.getFontMetrics(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 12));
            int w = metrics.stringWidth(text);
            int h = metrics.getHeight();
            g.setFont(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 12));
            g.drawString(text, (x + (64 / 2)) - (w / 2), yM + hM - (hM / 2) + (h / 2) - 3);
        }
    }
    
    /**
     * Peint un champignon sur un objet {@link java.awt.Graphics}
     * @param g Correspond au graphique (ou toile) de l'objet à repeindre
     * @param x Correspond au point sur l'axe des abscisses où sera peint le champignon
     * @param y Correspond au point sur l'axe des ordonnées où sera peint le champignon
     * @param color Correspond à la couleur du champignon
     */
    public final static void paintMushroom(java.awt.Graphics g, int x, int y, java.awt.Color color) {
        
        //PIED
        g.setColor(java.awt.Color.WHITE);
        java.awt.Polygon pied = new java.awt.Polygon();
        pied.addPoint(18 + x, 36 + y);
        pied.addPoint(45 + x, 36 + y);
        pied.addPoint(45 + x, 40 + y);
        pied.addPoint(46 + x, 40 + y);
        pied.addPoint(46 + x, 44 + y);
        pied.addPoint(47 + x, 44 + y);
        pied.addPoint(47 + x, 47 + y);
        pied.addPoint(48 + x, 47 + y);
        pied.addPoint(48 + x, 50 + y);
        pied.addPoint(49 + x, 50 + y);
        pied.addPoint(49 + x, 52 + y);
        pied.addPoint(50 + x, 52 + y);
        pied.addPoint(51 + x, 53 + y);
        pied.addPoint(51 + x, 55 + y);
        pied.addPoint(50 + x, 56 + y);
        pied.addPoint(49 + x, 56 + y);
        pied.addPoint(49 + x, 57 + y);
        pied.addPoint(47 + x, 57 + y);
        pied.addPoint(47 + x, 58 + y);
        pied.addPoint(45 + x, 58 + y);
        pied.addPoint(45 + x, 59 + y);
        pied.addPoint(39 + x, 59 + y);
        pied.addPoint(39 + x, 60 + y);
        pied.addPoint(24 + x, 60 + y);
        pied.addPoint(24 + x, 59 + y);
        pied.addPoint(18 + x, 59 + y);
        pied.addPoint(18 + x, 58 + y);
        pied.addPoint(16 + x, 58 + y);
        pied.addPoint(16 + x, 57 + y);
        pied.addPoint(14 + x, 57 + y);
        pied.addPoint(14 + x, 56 + y);
        pied.addPoint(13 + x, 56 + y);
        pied.addPoint(12 + x, 55 + y);
        pied.addPoint(12 + x, 53 + y);
        pied.addPoint(13 + x, 52 + y);
        pied.addPoint(14 + x, 52 + y);
        pied.addPoint(14 + x, 50 + y);
        pied.addPoint(15 + x, 50 + y);
        pied.addPoint(15 + x, 47 + y);
        pied.addPoint(16 + x, 47 + y);
        pied.addPoint(16 + x, 44 + y);
        pied.addPoint(17 + x, 44 + y);
        pied.addPoint(17 + x, 40 + y);
        pied.addPoint(18 + x, 40 + y);
        g.fillPolygon(pied);
        g.setColor(java.awt.Color.BLACK);
        g.drawPolygon(pied);
        
        g.setColor(new java.awt.Color(103, 59, 28));
        java.awt.Polygon porte = new java.awt.Polygon();
        porte.addPoint(30 + x, 44 + y);
        porte.addPoint(33 + x, 44 + y);
        porte.addPoint(36 + x, 47 + y);
        porte.addPoint(36 + x, 48 + y);
        porte.addPoint(37 + x, 49 + y);
        porte.addPoint(37 + x, 51 + y);
        porte.addPoint(38 + x, 51 + y);
        porte.addPoint(38 + x, 54 + y);
        porte.addPoint(39 + x, 54 + y);
        porte.addPoint(39 + x, 60 + y);
        porte.addPoint(24 + x, 60 + y);
        porte.addPoint(24 + x, 54 + y);
        porte.addPoint(25 + x, 54 + y);
        porte.addPoint(25 + x, 51 + y);
        porte.addPoint(26 + x, 51 + y);
        porte.addPoint(26 + x, 49 + y);
        porte.addPoint(27 + x, 48 + y);
        porte.addPoint(27 + x, 47 + y);
        g.fillPolygon(porte);
        g.setColor(java.awt.Color.BLACK);
        g.drawPolygon(porte);
        g.drawLine(25 + x, 55 + y, 25 + x, 55 + y);
        g.drawLine(38 + x, 55 + y, 38 + x, 55 + y);
        g.setColor(new java.awt.Color(49, 28, 14));
        g.drawLine(30 + x, 45 + y, 33 + x, 45 + y);
        g.drawLine(33 + x, 45 + y, 35 + x, 47 + y);
        g.drawLine(35 + x, 48 + y, 36 + x, 49 + y);
        g.drawLine(36 + x, 49 + y, 36 + x, 51 + y);
        g.drawLine(26 + x, 52 + y, 37 + x, 52 + y);
        g.drawLine(37 + x, 52 + y, 37 + x, 54 + y);
        g.drawLine(26 + x, 54 + y, 37 + x, 54 + y);
        g.drawLine(35 + x, 52 + y, 35 + x, 54 + y);
        g.drawLine(30 + x, 45 + y, 28 + x, 47 + y);
        g.drawLine(28 + x, 47 + y, 28 + x, 48 + y);
        g.drawLine(28 + x, 48 + y, 27 + x, 49 + y);
        g.drawLine(27 + x, 49 + y, 27 + x, 52 + y);
        g.drawLine(32 + x, 46 + y, 32 + x, 51 + y);
        g.drawLine(27 + x, 55 + y, 27 + x, 58 + y);
        g.drawLine(32 + x, 55 + y, 32 + x, 58 + y);
        g.drawLine(25 + x, 56 + y, 25 + x, 59 + y);
        g.drawLine(25 + x, 59 + y, 38 + x, 59 + y);
        g.drawLine(38 + x, 59 + y, 38 + x, 56 + y);
        g.setColor(new java.awt.Color(73, 41, 20));
        g.drawLine(26 + x, 53 + y, 34 + x, 53 + y);
        g.drawLine(36 + x, 53 + y, 36 + x, 53 + y);
        
        //CHAPEAU
        g.setColor(color);
        java.awt.Polygon chapeau = new java.awt.Polygon();
        chapeau.addPoint(27 + x, 0 + y);
        chapeau.addPoint(36 + x, 0 + y);
        chapeau.addPoint(36 + x, 1 + y);
        chapeau.addPoint(40 + x, 1 + y);
        chapeau.addPoint(40 + x, 2 + y);
        chapeau.addPoint(42 + x, 2 + y);
        chapeau.addPoint(42 + x, 3 + y);
        chapeau.addPoint(44 + x, 3 + y);
        chapeau.addPoint(44 + x, 4 + y);
        chapeau.addPoint(46 + x, 4 + y);
        chapeau.addPoint(46 + x, 5 + y);
        chapeau.addPoint(48 + x, 5 + y);
        chapeau.addPoint(48 + x, 6 + y);
        chapeau.addPoint(49 + x, 6 + y);
        chapeau.addPoint(49 + x, 7 + y);
        chapeau.addPoint(50 + x, 7 + y);
        chapeau.addPoint(50 + x, 8 + y);
        chapeau.addPoint(51 + x, 8 + y);
        chapeau.addPoint(51 + x, 9 + y);
        chapeau.addPoint(52 + x, 9 + y);
        chapeau.addPoint(52 + x, 10 + y);
        chapeau.addPoint(53 + x, 10 + y);
        chapeau.addPoint(53 + x, 12 + y);
        chapeau.addPoint(54 + x, 12 + y);
        chapeau.addPoint(54 + x, 13 + y);
        chapeau.addPoint(55 + x, 13 + y);
        chapeau.addPoint(55 + x, 15 + y);
        chapeau.addPoint(56 + x, 15 + y);
        chapeau.addPoint(56 + x, 17 + y);
        chapeau.addPoint(57 + x, 17 + y);
        chapeau.addPoint(57 + x, 20 + y);
        chapeau.addPoint(58 + x, 20 + y);
        chapeau.addPoint(58 + x, 24 + y);
        chapeau.addPoint(59 + x, 24 + y);
        chapeau.addPoint(59 + x, 28 + y);
        chapeau.addPoint(58 + x, 28 + y);
        chapeau.addPoint(58 + x, 30 + y);
        chapeau.addPoint(57 + x, 30 + y);
        chapeau.addPoint(57 + x, 32 + y);
        chapeau.addPoint(56 + x, 32 + y);
        chapeau.addPoint(56 + x, 33 + y);
        chapeau.addPoint(55 + x, 33 + y);
        chapeau.addPoint(55 + x, 34 + y);
        chapeau.addPoint(53 + x, 34 + y);
        chapeau.addPoint(53 + x, 35 + y);
        chapeau.addPoint(51 + x, 35 + y);
        chapeau.addPoint(51 + x, 36 + y);
        chapeau.addPoint(12 + x, 36 + y);
        chapeau.addPoint(12 + x, 35 + y);
        chapeau.addPoint(10 + x, 35 + y);
        chapeau.addPoint(10 + x, 34 + y);
        chapeau.addPoint(8 + x, 34 + y);
        chapeau.addPoint(8 + x, 33 + y);
        chapeau.addPoint(7 + x, 33 + y);
        chapeau.addPoint(7 + x, 32 + y);
        chapeau.addPoint(6 + x, 32 + y);
        chapeau.addPoint(6 + x, 30 + y);
        chapeau.addPoint(5 + x, 30 + y);
        chapeau.addPoint(5 + x, 28 + y);
        chapeau.addPoint(4 + x, 28 + y);
        chapeau.addPoint(4 + x, 24 + y);
        chapeau.addPoint(5 + x, 24 + y);
        chapeau.addPoint(5 + x, 20 + y);
        chapeau.addPoint(6 + x, 20 + y);
        chapeau.addPoint(6 + x, 17 + y);
        chapeau.addPoint(7 + x, 17 + y);
        chapeau.addPoint(7 + x, 15 + y);
        chapeau.addPoint(8 + x, 15 + y);
        chapeau.addPoint(8 + x, 13 + y);
        chapeau.addPoint(9 + x, 13 + y);
        chapeau.addPoint(9 + x, 12 + y);
        chapeau.addPoint(10 + x, 12 + y);
        chapeau.addPoint(10 + x, 10 + y);
        chapeau.addPoint(11 + x, 10 + y);
        chapeau.addPoint(11 + x, 9 + y);
        chapeau.addPoint(12 + x, 9 + y);
        chapeau.addPoint(12 + x, 8 + y);
        chapeau.addPoint(13 + x, 8 + y);
        chapeau.addPoint(13 + x, 7 + y);
        chapeau.addPoint(14 + x, 7 + y);
        chapeau.addPoint(14 + x, 6 + y);
        chapeau.addPoint(15 + x, 6 + y);
        chapeau.addPoint(15 + x, 5 + y);
        chapeau.addPoint(17 + x, 5 + y);
        chapeau.addPoint(17 + x, 4 + y);
        chapeau.addPoint(19 + x, 4 + y);
        chapeau.addPoint(19 + x, 3 + y);
        chapeau.addPoint(21 + x, 3 + y);
        chapeau.addPoint(21 + x, 2 + y);
        chapeau.addPoint(23 + x, 2 + y);
        chapeau.addPoint(23 + x, 1 + y);
        chapeau.addPoint(27 + x, 1 + y);
        g.fillPolygon(chapeau);
        g.setColor(java.awt.Color.BLACK);
        g.drawPolygon(chapeau);
        
        g.setColor(new java.awt.Color(89, 80, 74));
        java.awt.Polygon window = new java.awt.Polygon();
        window.addPoint(39 + x, 39 + y);
        window.addPoint(41 + x, 39 + y);
        window.addPoint(43 + x, 41 + y);
        window.addPoint(43 + x, 43 + y);
        window.addPoint(41 + x, 45 + y);
        window.addPoint(39 + x, 45 + y);
        window.addPoint(37 + x, 43 + y);
        window.addPoint(37 + x, 41 + y);
        g.fillPolygon(window);
        g.setColor(java.awt.Color.BLACK);
        g.drawPolygon(window);
        g.drawLine(40 + x, 39 + y, 40 + x, 45 + y);
        g.drawLine(37 + x, 42 + y, 43 + x, 42 + y);
        
        //POINTS
        g.setColor(java.awt.Color.WHITE);
        java.awt.Polygon p1 = new java.awt.Polygon();
        p1.addPoint(25 + x, 4 + y);
        p1.addPoint(29 + x, 4 + y);
        p1.addPoint(31 + x, 6 + y);
        p1.addPoint(31 + x, 7 + y);
        p1.addPoint(28 + x, 10 + y);
        p1.addPoint(25 + x, 10 + y);
        p1.addPoint(23 + x, 7 + y);
        p1.addPoint(23 + x, 6 + y);
        g.fillPolygon(p1);
        
        java.awt.Polygon p2 = new java.awt.Polygon();
        p2.addPoint(39 + x, 6 + y);
        p2.addPoint(41 + x, 6 + y);
        p2.addPoint(42 + x, 7 + y);
        p2.addPoint(42 + x, 8 + y);
        p2.addPoint(40 + x, 10 + y);
        p2.addPoint(39 + x, 10 + y);
        p2.addPoint(38 + x, 8 + y);
        p2.addPoint(38 + x, 7 + y);
        g.fillPolygon(p2);
        
        java.awt.Polygon p3 = new java.awt.Polygon();
        p3.addPoint(47 + x, 11 + y);
        p3.addPoint(49 + x, 13 + y);
        p3.addPoint(49 + x, 14 + y);
        p3.addPoint(47 + x, 16 + y);
        p3.addPoint(46 + x, 14 + y);
        p3.addPoint(46 + x, 13 + y);
        g.fillPolygon(p3);
        
        java.awt.Polygon p4 = new java.awt.Polygon();
        p4.addPoint(11 + x, 24 + y);
        p4.addPoint(13 + x, 24 + y);
        p4.addPoint(14 + x, 25 + y);
        p4.addPoint(12 + x, 27 + y);
        p4.addPoint(11 + x, 27 + y);
        p4.addPoint(10 + x, 25 + y);
        g.fillPolygon(p4);
        
        java.awt.Polygon p5 = new java.awt.Polygon();
        p5.addPoint(15 + x, 11 + y);
        p5.addPoint(17 + x, 11 + y);
        p5.addPoint(19 + x, 13 + y);
        p5.addPoint(19 + x, 16 + y);
        p5.addPoint(17 + x, 18 + y);
        p5.addPoint(15 + x, 19 + y);
        p5.addPoint(13 + x, 16 + y);
        p5.addPoint(13 + x, 13 + y);
        g.fillPolygon(p5);
        
        java.awt.Polygon p6 = new java.awt.Polygon();
        p6.addPoint(28 + x, 14 + y);
        p6.addPoint(30 + x, 14 + y);
        p6.addPoint(31 + x, 15 + y);
        p6.addPoint(31 + x, 16 + y);
        p6.addPoint(30 + x, 17 + y);
        p6.addPoint(28 + x, 18 + y);
        p6.addPoint(27 + x, 16 + y);
        p6.addPoint(27 + x, 15 + y);
        g.fillPolygon(p6);
        
        java.awt.Polygon p7 = new java.awt.Polygon();
        p7.addPoint(22 + x, 26 + y);
        p7.addPoint(25 + x, 26 + y);
        p7.addPoint(28 + x, 29 + y);
        p7.addPoint(28 + x, 31 + y);
        p7.addPoint(24 + x, 35 + y);
        p7.addPoint(22 + x, 35 + y);
        p7.addPoint(19 + x, 31 + y);
        p7.addPoint(19 + x, 29 + y);
        g.fillPolygon(p7);
        
        java.awt.Polygon p8 = new java.awt.Polygon();
        p8.addPoint(37 + x, 20 + y);
        p8.addPoint(41 + x, 20 + y);
        p8.addPoint(44 + x, 23 + y);
        p8.addPoint(44 + x, 25 + y);
        p8.addPoint(40 + x, 29 + y);
        p8.addPoint(37 + x, 29 + y);
        p8.addPoint(34 + x, 25 + y);
        p8.addPoint(34 + x, 23 + y);
        g.fillPolygon(p8);

        java.awt.Polygon p9 = new java.awt.Polygon();
        p9.addPoint(51 + x, 23 + y);
        p9.addPoint(54 + x, 23 + y);
        p9.addPoint(56 + x, 25 + y);
        p9.addPoint(56 + x, 29 + y);
        p9.addPoint(53 + x, 32 + y);
        p9.addPoint(51 + x, 32 + y);
        p9.addPoint(49 + x, 29 + y);
        p9.addPoint(49 + x, 25 + y);
        g.fillPolygon(p9);
    }
    
    /**
     * Peint un mouvement d'unités d'un champignon à destination d'un autre champignon sur un objet {@link java.awt.Graphics}
     * @param g Correspond au graphique (ou toile) de l'objet à repeindre
     * @param x Correspond au point sur l'axe des abscisses où sera peint les unités
     * @param y Correspond au point sur l'axe des ordonnées où sera peint les unités
     * @param countUnits Correspond au nombres d'unités à dessiner
     * @param color Correspond à la couleur des unités en déplacement
     */
    public final static void paintUnit(java.awt.Graphics g, int x, int y, int countUnits, java.awt.Color color) {
        switch (countUnits) {
            case 1:
                g.setColor(color);
                g.fillOval(x - 5 + 32, y - 5 + 60, 10, 10);
                g.setColor(java.awt.Color.WHITE);
                g.drawOval(x - 5 + 32, y - 5 + 60, 10, 10);
                break;
            case 2:
                g.setColor(color);
                g.fillOval(x - 5 + 29, y - 5 + 60, 10, 10);
                g.setColor(java.awt.Color.WHITE);
                g.drawOval(x - 5 + 29, y - 5 + 60, 10, 10);
                g.setColor(color);
                g.fillOval(x - 5 + 35, y - 5 + 60, 10, 10);
                g.setColor(java.awt.Color.WHITE);
                g.drawOval(x - 5 + 35, y - 5 + 60, 10, 10);
                break;
            default:
                g.setColor(color);
                g.fillOval(x - 5 + 29, y - 5 + 60, 10, 10);
                g.setColor(java.awt.Color.WHITE);
                g.drawOval(x - 5 + 29, y - 5 + 60, 10, 10);
                g.setColor(color);
                g.fillOval(x - 5 + 35, y - 5 + 60, 10, 10);
                g.setColor(java.awt.Color.WHITE);
                g.drawOval(x - 5 + 35, y - 5 + 60, 10, 10);
                g.setColor(color);
                g.fillOval(x - 5 + 32, y - 5 + 57, 10, 10);
                g.setColor(java.awt.Color.WHITE);
                g.drawOval(x - 5 + 32, y - 5 + 57, 10, 10);
                break;
        }
    }
    
    
    
}