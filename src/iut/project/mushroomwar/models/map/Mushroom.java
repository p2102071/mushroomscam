/*
 * Copyright (C) 2022 IUT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package iut.project.mushroomwar.models.map;



import iut.project.mushroomwar.models.player.Player;
import iut.project.mushroomwar.models.player.Human;



/**
 * Cette classe représente les objets "Champignons"
 * @author Joseph BRIGUET
 * @version 1.0
 */
public final class Mushroom implements java.io.Serializable {
    
    
    
//ATTRIBUTS
    /**
     * Correspond au joueur qui possède la champignon
     */
    private Player player;
    
    /**
     * Correspond au nombre d'unités dans le champignon
     */
    private int countUnits;
    
    /**
     * Correspond à la position du champignon sur l'axe des abscisses
     */
    private int xPos;
    
    /**
     * Correspond à la position du champignon sur l'axe des ordonnées
     */
    private int yPos;
    
    /**
     * Détermine si le champignon est sélectionné ou pas par l'utilisateur {@link Human}
     */
    private boolean selected;

    
    
//CONSTRUCTORS
    /**
     * Crée un champignon
     * @param xPos Correspond à la position du champignon sur l'axe des abscisses
     * @param yPos Correspond à la position du champignon sur l'axe des ordonnées
     */
    public Mushroom(int xPos, int yPos) {
        this(null, xPos, yPos);
    }

    /**
     * Crée un champignon
     * @param player Correspond au joueur qui possède la champignon
     * @param xPos Correspond à la position du champignon sur l'axe des abscisses
     * @param yPos Correspond à la position du champignon sur l'axe des ordonnées
     */
    public Mushroom(Player player, int xPos, int yPos) {
        this.countUnits = (player != null) ? 1 : 0;
        this.player     = player;
        this.xPos       = xPos;
        this.yPos       = yPos;
    }
    
    
    
//METHODES PUBLICS
    /**
     * Renvoie le joueur propriétaire de ce champignon
     * @return Retourne le joueur propriétaire de ce champignon
     */
    public final Player getOwner() {
        return this.player;
    }
    
    /**
     * Modifie le joueur propriétaire de ce champignon
     * @param player Correspond au nouveau joueur propriétaire de ce champignon
     */
    public final void setOwner(Player player) {
        this.player = player;
    }
    
    /**
     * Renvoie la couleur du champignon par rapport à la couleur de l'équipe qui le possède
     * @return Retourne la couleur du champignon par rapport à la couleur de l'équipe qui le possède
     */
    public final java.awt.Color getTeamColor() {
        return (player == null) ? null : player.getColor();
    }

    /**
     * Renvoie la position du champignon sur la {@link Map}
     * @return Retourne la position du champignon sur la {@link Map}
     */
    public final java.awt.Point getPosition() {
        return new java.awt.Point(xPos, yPos);
    }
    
    /**
     * Ajoute une unité au champignon
     */
    public final void addUnit(){
        this.countUnits++;
    }
    
    /**
     * Ajoute n unités au champignon
     * @param n Correspond au nombre d'unité à ajouter au champignon
     */
    public final void addUnits(int n){
        this.countUnits += n;
    }
    
    /**
     * Modifie le nombre d'unités dans le champignon
     * @param n Correspond au nombre d'unités dans le champignon
     */
    public final void setUnits(int n){
        this.countUnits = n;
    }
    
    /**
     * Enlève une unité du champignon
     */
    public final void removeUnit(){
        this.countUnits--;
    }
    
    /**
     * Enlève n unités du champignon
     * @param n Correspond au nombre d'unité à enlever du champignon
     */
    public final void removeUnits(int n){
        this.countUnits -= n;
    }
    
    /**
     * Détermine si oui ou non le champignon appartient au joueur en paramètre
     * @param player Correspond au joueur dont on cherche à détermine si le champignon lui appartient
     * @return Retourne true si le champignon lui appartient, sinon false
     */
    public final boolean owns(Player player) {
        if(this.player == null || player == null) return false;
        else return this.player.equals(player);
    }
    
    /**
     * Détermine si le champignon ne contient aucune unité (Autrement dit, détermine si le champignon appartient à un joueur)
     * @return Retourne true, si le champignon appartient à personne, sinon false
     */
    public final boolean isEmpty(){
        return this.player == null || this.countUnits == 0;
    }
    
    /**
     * Renvoie le nombre d'unités dans la champignon
     * @return Retourne le nombre d'unités dans le champignon
     */
    public final int countUnits(){
        return this.countUnits;
    }
    
    /**
     * Détermine si le champignon est sélectionné ou pas
     * @return Retourne true si c'est le cas, sinon false
     */
    public final boolean isSelected(){
        return this.selected;
    }
    
    /**
     * Change la sélection du champignon
     * @param select True si le champignon doit être sélectionné ou false sinon
     */
    public final void setSelected(boolean select){
        this.selected = select;
    }

    /**
     * Renvoie le hashCode du Champignon
     * @return Retourne le hashCode du Champignon
     */
    @Override
    public final int hashCode() {
        int hash = 5;
        hash = 53 * hash + this.xPos;
        hash = 53 * hash + this.yPos;
        return hash;
    }

    /**
     * Détermine si deux champignons sont identiques ou pas
     * @param obj Correspond au second champignon à comparer au courant
     * @return Retourne true si les deux champignons sont identiques, sinon false
     */
    @Override
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mushroom other = (Mushroom) obj;
        if (this.xPos != other.xPos) {
            return false;
        }
        return this.yPos == other.yPos;
    }
    
    
    
}