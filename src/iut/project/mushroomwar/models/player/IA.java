/*
 * Copyright (C) 2022 IUT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package iut.project.mushroomwar.models.player;



import iut.project.mushroomwar.models.Level;



/**
 * Cette classe représente les objets "Joueur IA"
 * @author Joseph BRIGUET
 * @version 1.0
 */
public class IA extends Player{

    
    
//ATTRIBUTS
    /**
     * Correspond au niveau de difficulté de l'{@link IA}
     */
    private Level level;
    
    
    
//CONSTRUCTOR
    /**
     * Crée une IA
     * @param name Correspond au nom de l'IA
     * @param color Correspond à la couleur de l'IA
     * @param level Correspond au niveau de l'IA
     */
    public IA(String name, java.awt.Color color, Level level) {
        super(name, color);
        this.level = level;
    }

    
    
//METHODES PUBLICS
    /**
     * Renvoie le niveau de difficulté de l'{@link IA}
     * @return Retourne le niveau de difficulté de l'{@link IA}
     */
    public final Level getLevel() {
        return level;
    }

    /**
     * Modifie le niveau de difficulté de l'{@link IA}
     * @param level Correspond au niveau de difficulté de l'{@link IA}
     */
    public final void setLevel(Level level) {
        this.level = level;
    }
    
    
    
}