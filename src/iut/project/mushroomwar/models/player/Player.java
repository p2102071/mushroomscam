/*
 * Copyright (C) 2022 IUT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package iut.project.mushroomwar.models.player;



/**
 * Cette classe représente les objets {@link Player} pouvant intéragir avec le jeu
 * @author Joseph BRIGUET
 * @version 1.0
 */
public abstract class Player implements java.io.Serializable {
    
    
    
//ATTRIBUT STATIC
    /**
     * Correspond au compteur de création de {@link Player}
     */
    private static int counterID = 0;
    
    
    
//ATTRIBUTS
    /**
     * Correspond à l'id unique du joueur
     */
    private int id;
    
    /**
     * Correspond au prénom du joueur
     */
    private String firstname;
    
    /**
     * Correspond à la couleur du joueur
     */
    private java.awt.Color color;

    
    
//CONSTRUCTORS
    /**
     * Crée un joueur
     */
    public Player() {
    }
    
    /**
     * Crée un joueur
     * @param firstname Correspond au prénom du joueur
     * @param color Correspond à la couleur du joueur
     */
    public Player(String firstname, java.awt.Color color) {
        this.id         = counterID++;
        this.firstname  = firstname;
        this.color      = color;
    }

    
    
//GETTERS & SETTERS
    /**
     * Renvoie le prénom du joueur
     * @return Retourne le prénom du joueur
     */
    public final String getFirstname() {
        return firstname;
    }

    /**
     * Modifie le prénom du joueur
     * @param firstname Correspond au nouveau prénom du joueur
     */
    public final void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * Renvoie la couleur du joueur
     * @return Retourne la couleur du joueur
     */
    public final java.awt.Color getColor() {
        return color;
    }

    /**
     * Modifie la couleur du joueur
     * @param color Correspond à la nouvelle couleur du joueur
     */
    public final void setColor(java.awt.Color color) {
        this.color = color;
    }

    
    
//METHODES PUBLICS
    /**
     * Renvoie le hashCode du {@link Player}
     * @return Retourne le hashCode du {@link Player}
     */
    @Override
    public final int hashCode() {
        int hash = 3;
        hash = 89 * hash + this.id;
        return hash;
    }
    
    /**
     * Détermine si deux {@link Player} sont identiques ou pas
     * @param obj Correspond au second {@link Player} à comparer au courant
     * @return Retourne true, s'ils sont identiques, sinon false
     */
    @Override    
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Player other = (Player) obj;
        return this.id == other.id;
    }

    /**
     * Renvoie un {@link Player} sous la forme d'une chaîne de caractères
     * @return Retourne un {@link Player} sous la forme d'une chaîne de caractères
     */
    @Override
    public final String toString() {
        return firstname;
    }
    
    
    
}