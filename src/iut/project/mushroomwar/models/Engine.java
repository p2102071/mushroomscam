package iut.project.mushroomwar.models;

import iut.project.mushroomwar.Pulsator;
import iut.project.mushroomwar.assets.Drawer;
import iut.project.mushroomwar.models.interfaces.MapDesigner;
import iut.project.mushroomwar.models.map.Map;
import iut.project.mushroomwar.models.map.Mushroom;
import iut.project.mushroomwar.models.player.Human;
import iut.project.mushroomwar.models.player.Player;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Elliot
 */
public class Engine extends Pulsator{
    
    private final List<Mushroom> mushrooms;
    private Map map;
    private Player human;
    private Color neutralColor;
    private final List<Player> players;
    private int countdownGameStarting;
    private boolean gameStarted;

    public Engine(Human human,Map map,List<Player> players){
        this(human,map,new Color(141,141,141),players);
    }
    
    public Engine(Human human,Map map,Color neutralColor,List<Player> players){
        this.mushrooms = new ArrayList<>();
        this.neutralColor = neutralColor;
        this.players = players;
        this.human = human;
        this.map = map;
        this.map.setMouseListener(new MouseAdapter(){
            @Override
            public void mouseMoved(MouseEvent e) {
                Engine.this.mouseMoved(e); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                Engine.this.mouseClicked(e);  // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
            }
            
        });
        
        this.map.setDesignerListener(new MapDesigner(){
            @Override
            public void paintMushrooms(Graphics g) {
                Engine.this.paintMushrooms(g);
            }

            @Override
            public void paintMushroomLabels(Graphics g) {
                Engine.this.paintMushroomLabels(g);
            }

            @Override
            public void paintMushroomSelected(Graphics g) {
                Engine.this.paintMushroomSelected(g);
            }

            @Override
            public void paintMovement(Graphics g) {
                Engine.this.paintMovement(g);
            }

            @Override
            public void paintCountdown(Map map, Graphics g) {
                Engine.this.paintCountdown(Engine.this.map,g);
            }
        });
        
        this.countdownGameStarting = 5;
        
    }

    public boolean isGameStarted() {
        return gameStarted;
    }

    public void setGameStarted(boolean gameStarted) {
        this.gameStarted = gameStarted;
    }
    
    public void addMushroom(Mushroom m){
        if(m != null){
            mushrooms.add(m);
        }
        
    }

    @Override
    protected void tick(long cpt, int tps) {
        if(isGameStarted()){
            
        }else{
            if(cpt % 100 == 0){
                this.map.repaint();
            }
        }
    }
    
    private void paintMushrooms(Graphics g){
        for(Mushroom m : this.mushrooms){
            Drawer.paintMushroom(g, m.getPosition().x, m.getPosition().y, m.getTeamColor() == null ? this.neutralColor : m.getTeamColor());
        }
    }
    
    private void paintMushroomLabels(Graphics g) {
        
    }
    
    private void paintMushroomSelected(Graphics g) {
        
    }
    
    private void paintMovement(Graphics g) {
        
    }
    
    public void paintCountdown(Map map, Graphics g) {
        
    }
    
    private void mouseMoved(MouseEvent e){
        
    }
    
    private void mouseClicked(MouseEvent e){
        
    }
}
