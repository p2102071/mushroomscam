/*
 * Copyright (C) 2022 IUT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package iut.project.mushroomwar.models;




/**
 * Cette class énumère les différents niveaux de difficultés des {@link IA}. Le taux de réactivité est un nombre sur un interval défini [{@link #minReactivity}; {@link #maxReactivity}]. Ce nombre représente le nombre de millisecondes avant que l'{@link IA} décide de calculer un nouveau coup d'attaque
 * @author Joseph BRIGUET
 * @version 1.0
 */
public enum Level {
    
    
    
//CONSTANTES
    /**
     * Mauvais niveau d'une {@link IA}
     */
    BAD("Facile", 80, 175),
    
    /**
     * Moyen niveau d'une {@link IA}
     */
    MEDIUM("Moyen", 50, 125),
    
    /**
     * Bon niveau d'une {@link IA}
     */
    HIGH("Difficile", 20, 75),
    
    /**
     * Excellent niveau d'une {@link IA}
     */
    EXTREME("Hard", 10, 25);
    
    
    
//ATTRIBUTS
    /**
     * Correspond au nom du niveau
     */
    private final String level;
    
    /**
     * Correspond à la valeur minimum de réactivité pour ce niveau
     */
    private final int minReactivity;
    
    /**
     * Correspond à la valeur maximum de réactivité pour ce niveau
     */
    private final int maxReactivity;

    
    
//CONSTRUCTOR
    /**
     * Crée un niveau de difficulté d'une {@link IA}
     * @param levelName Correspond au nom du niveau
     * @param minReactivity Correspond à la valeur minimum de réactivité pour ce niveau
     * @param maxReactivity Correspond à la valeur maximum de réactivité pour ce niveau
     */
    private Level(String levelName, int minReactivity, int maxReactivity) {
        this.level = levelName;
        this.minReactivity = minReactivity;
        this.maxReactivity = maxReactivity;
    }

    
    
//METHODES PUBLICS
    /**
     * Renvoie la valeur minimum de réactivité pour ce niveau
     * @return Retourne la valeur minimum de réactivité pour ce niveau
     */
    public final int getMinReactivity() {
        return minReactivity;
    }

    /**
     * Renvoie la valeur maximum de réactivité pour ce niveau
     * @return Retourne la valeur maximum de réactivité pour ce niveau
     */
    public final int getMaxReactivity() {
        return maxReactivity;
    }

    /**
     * Renvoie le niveau sous la forme d'une chaîne de caractères
     * @return Retourne le niveau sous la forme d'une chaîne de caractères
     */
    @Override
    public final String toString() {
        return level;
    }
    
    
    
}