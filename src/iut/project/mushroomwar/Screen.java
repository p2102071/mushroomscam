/*
 * Copyright (C) 2022 IUT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package iut.project.mushroomwar;



import iut.project.mushroomwar.assets.Drawer;
import iut.project.mushroomwar.assets.SoundPlayer;
import iut.project.mushroomwar.models.Engine;
import iut.project.mushroomwar.models.map.Mushroom;
import iut.project.mushroomwar.models.player.Human;
import iut.project.mushroomwar.models.player.Player;
import iut.project.mushroomwar.ui.PanelInit;
import iut.project.mushroomwar.ui.PanelName;
import java.awt.EventQueue;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



/**
 * Cette classe représente l'objet fenêtre principale de l'application. Il n'y a que cet écran dans cette application
 * @author Joseph BRIGUET
 * @version 1.0
 */
public class Screen extends javax.swing.JFrame implements PanelName.CardNameListener, PanelInit.CardInitListener {

    private Engine engine;
    
//CONSTRUCTOR
    /**
     * Crée la fenêtre principale de l'application
     */
    public Screen() {
        initComponents();
        adjustFrameSize();
        SoundPlayer.ambiance();
        setLocationRelativeTo(null);
    }
    
    
    
//METHODES PRIVATES
    
    private void initEngine(Human human, java.util.List<Player> players){
        this.engine = new Engine(human,this.map,players);
        this.engine.start();
    }
    
    private void initMap(List<Player> players){
        List<Player> playersCopy = new ArrayList<>(players);
        Collections.shuffle(playersCopy);
        List<Point> locations = getMushroomsLocations();
        
        while(!playersCopy.isEmpty()){
            this.engine.addMushroom(createMushroom(playersCopy.remove(0),locations.remove(0)));
        }
        
        for(Point location : locations){
            this.engine.addMushroom(createMushroom(null,location));
        }
        
    }
    
    private Mushroom createMushroom(Player player,Point location){
        return new Mushroom(player,location.x,location.y);
    }
    
    private List<Point> getMushroomsLocations(){
        List<Point> locations = new ArrayList<>();
        locations.add(new Point(30,40));
        locations.add(new Point(970,520));
        locations.add(new Point(300,520));
        locations.add(new Point(28,334));
        locations.add(new Point(7,495));
        locations.add(new Point(246,242));
        locations.add(new Point(324,90));
        locations.add(new Point(640,5));
        locations.add(new Point(966,66));
        locations.add(new Point(591,211));
        locations.add(new Point(890,348));
        locations.add(new Point(774,140));
        locations.add(new Point(632,346));
        locations.add(new Point(476,520));
        locations.add(new Point(738,462));
        Collections.shuffle(locations);
        return locations;
    }
    
    /**
     * Rend la carte CARD_NAME visible
     */
    private void toCardName() {
        changeCard_cardLayout("CARD_NAME");
        this.cardInit.init(cardName.getName());
        this.map.reset();
    }
    
    /**
     * Rend la carte CARD_INIT visible
     * @param nameHuman Correspond au prénom du joueur humain
     */
    private void toCardInit(String nameHuman) {
        changeCard_cardLayout("CARD_INIT");
        this.cardInit.init(nameHuman);
    }
    
    /**
     * Rend la carte CARD_RESULT visible
     */
    private void toCardResult() {
        this.mainPanel.setVisible(true);
        changeCard_cardLayout("CARD_RESULT");
    }
    
    /**
     * Rend une carte visible pour le cardLayout de l'objet {@link #mainPanel}
     * @param nameCard Correspond au nom de la carte à rendre visible
     * @return Retourne la carte visible
     */
    private void changeCard_cardLayout(String nameCard){
        java.awt.CardLayout layout = (java.awt.CardLayout) mainPanel.getLayout();
        layout.show(mainPanel, nameCard);
    }
    
    private void adjustFrameSize(){
        EventQueue.invokeLater(new Runnable(){
            @Override
            public void run() {
                int diffX = Screen.this.getContentPane().getSize().width - Screen.this.map.getSize().width;
                int diffY = Screen.this.getContentPane().getSize().height - Screen.this.map.getSize().height;
                Screen.this.setSize(Screen.this.getSize().width - diffX,Screen.this.getSize().height - diffY);
            }
        });
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        map = new iut.project.mushroomwar.models.map.Map();
        mainPanel = new javax.swing.JPanel();
        cardName = new iut.project.mushroomwar.ui.PanelName();
        cardInit = new iut.project.mushroomwar.ui.PanelInit();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Mushroom War");
        setIconImage(Drawer.getAppIcon());
        setResizable(false);

        mainPanel.setOpaque(false);
        mainPanel.setLayout(new java.awt.CardLayout());

        cardName.setNameListener(this);
        mainPanel.add(cardName, "CARD_NAME");

        cardInit.setInitListener(this);
        mainPanel.add(cardInit, "CARD_INIT");

        javax.swing.GroupLayout mapLayout = new javax.swing.GroupLayout(map);
        map.setLayout(mapLayout);
        mapLayout.setHorizontalGroup(
            mapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mapLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 1075, Short.MAX_VALUE)
                .addContainerGap())
        );
        mapLayout.setVerticalGroup(
            mapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mapLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(map, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(map, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private iut.project.mushroomwar.ui.PanelInit cardInit;
    private iut.project.mushroomwar.ui.PanelName cardName;
    private javax.swing.JPanel mainPanel;
    private iut.project.mushroomwar.models.map.Map map;
    // End of variables declaration//GEN-END:variables


    
//EVTS
    /**
     * Lorsque le joueur communique son prénom
     * @param nameHuman Correspond au prénom du joueur humain
     */
    @Override
    public final void evtCardName(String nameHuman) {
        toCardInit(nameHuman);
    }

    /**
     * Lorsque l'utilisateur a cliqué sur le bouton Jouer
     * @param human Correspond au joueur humain. Celui qui lance la partie
     * @param players Correspond à la liste des joueurs. Le joueur humain est compris dedans
     */
    @Override
    public final void evtCardInit(Human human, java.util.List<Player> players) {
        initEngine(human,players);
        initMap(players);
        this.mainPanel.setVisible(false);
    }    
}