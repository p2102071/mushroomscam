/*
 * Copyright (C) 2022 IUT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package iut.project.mushroomwar.ui;



/**
 * Cette classe représente un {@link javax.swing.JPanel} permettant au joueur de saisir son prénom
 * @author Joseph BRIGUET
 * @version 1.0
 */
public final class PanelName extends PanelAlpha {
    
    
    
//CONSTANTES
    /**
     * Correspond à l'espace entre le {@link #nameLabel} et le {@link #nameTextField}
     */
    private final static int GAP_BETWEEN_COMPONENTS = 10;
    
    /**
     * Correspond au nom du fichier où sera stocké le prénom de l'utilisateur et ce dans le but de lui éviter de le retaper à chaque fois
     */
    private final static String FILE_NAME = "player";

    
    
//ATTRIBUTS
    /**
     * Correspond au label d'instruction de saisie
     */
    private javax.swing.JLabel nameLabel;
    
    /**
     * Correspond au champ de saisie du prénom
     */
    private javax.swing.JTextField nameTextField;
    
    /**
     * Correspond au bouton qui permet de valider la saisie
     */
    private javax.swing.JButton nextButton;
    
    /**
     * Correspond au bouton qui permet de quitter l'application
     */
    private javax.swing.JButton quitButton;
    
    /**
     * Correspond à l'objet qui écoutera le prénom saisi par le joueur
     */
    private CardNameListener listener;
    
    
    
//CONSTRUCTOR
    /**
     * Crée le panneau {@link PanelName}
     */
    public PanelName() {
        super();
        init();
    }

    
    
//GETTER & SETTER
    /**
     * Renvoie l'objet qui écoutera le prénom saisi par le joueur
     * @return Retourne l'objet qui écoutera le prénom saisi par le joueur
     */
    public final CardNameListener getNameListener() {
        return listener;
    }

    /**
     * Modifie l'objet qui écoutera le prénom saisi par le joueur
     * @param listener Correspond au nouvel objet qui écoutera le prénom saisi par le joueur
     */
    public final void setNameListener(CardNameListener listener) {
        this.listener = listener;
    }
    
    
    
//METHODE PROTECTED
    /**
     * Peint l'objet {@link PanelName}
     * @param g Correspond au graphique (ou toile) de l'objet à repeindre
     */
    @Override
    protected final void paintComponent(java.awt.Graphics g) {
        super.paintComponent(g);
        
        nameTextField.setLocation(
                halfWidth()  - halfWidth(nameTextField), 
                halfHeight() - halfHeight(nameTextField)
        );
        
        nameLabel.setLocation(
                halfWidth()  - halfWidth(nameTextField), 
                halfHeight() - halfHeight(nameTextField) - height(nameLabel) - GAP_BETWEEN_COMPONENTS
        );
        
        nextButton.setLocation(
                width()  - width(nextButton)  - PADDING, 
                height() - height(nextButton) - PADDING
        );
        
        quitButton.setLocation(
                PADDING, 
                height() - height(nextButton) - PADDING
        );
    }
    
    
    
//METHODES PRIVATES
    /**
     * Initialise la vue du {@link PanelName}
     */
    private void init() {
        java.awt.event.ActionListener action = (java.awt.event.ActionEvent e) -> {
            if(this.listener != null){
                writePlayerName();
                this.listener.evtCardName(nameTextField.getText());
            }
        };
        
        nameTextField   = new javax.swing.JTextField();
        nameLabel       = new javax.swing.JLabel();
        nextButton      = new javax.swing.JButton();
        quitButton      = new javax.swing.JButton();
        
        nameTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        nameTextField.setToolTipText("Votre prénom");
        nameTextField.addActionListener(action);
        
        nextButton.addActionListener(action);

        quitButton.addActionListener((java.awt.event.ActionEvent e) -> {
            System.exit(0);
        });
        
        nameLabel.setText("Votre prénom:");
        nextButton.setText("Suivant");
        quitButton.setText("Quitter");
        
        java.awt.Font font = new java.awt.Font("Segoe UI", 0, 14);
        nameTextField.setFont(new java.awt.Font("Segoe UI", 2, 24));
        nameLabel.setFont(font);
        nextButton.setFont(font);
        quitButton.setFont(font);
        
        nameTextField.setSize(540, 50);
        nameLabel.setSize(150, 20);
        nextButton.setSize(130, 50);
        quitButton.setSize(130, 50);
        
        add(nameTextField);
        add(nameLabel);
        add(nextButton);
        add(quitButton);
        
        readPlayerName();
    }
    
    /**
     * Ecrit le nom du joueur dans un fichier
     */
    private void writePlayerName() {
        try {
            try (java.io.BufferedWriter bw = new java.io.BufferedWriter(new java.io.FileWriter(new java.io.File(FILE_NAME)))) {
                bw.write(nameTextField.getText());
                bw.flush();
            }
        } catch (java.io.IOException ex) {
            java.util.logging.Logger.getLogger(PanelName.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Lit le nom du joueur précédemment enregistré
     */
    private void readPlayerName(){
        try {
            try (java.io.BufferedReader br = new java.io.BufferedReader(new java.io.FileReader(new java.io.File(FILE_NAME)))) {
                nameTextField.setText(br.readLine());
            }
        } catch (java.io.FileNotFoundException ex) {} catch (java.io.IOException ex) {
            java.util.logging.Logger.getLogger(PanelName.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    
    
    
//INTERFACE
    /**
     * Cette interface permet de faire le lien entre un objet et un objet {@link PanelName}
     * @author Joseph BRIGUET
     * @version 1.0
     */
    public interface CardNameListener {



        /**
         * Lorsque le joueur communique son prénom
         * @param nameHuman Correspond au prénom du joueur humain
         */
        public void evtCardName(String nameHuman);



    }
    
    
    
}