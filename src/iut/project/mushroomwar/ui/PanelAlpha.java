/*
 * Copyright (C) 2022 IUT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package iut.project.mushroomwar.ui;



/**
 * Cette classe représente un objet {@link javax.swing.JPanel} dont le background est un peu stylisé
 * @author Joseph BRIGUET
 * @version 1.0
 */
public class PanelAlpha extends javax.swing.JPanel {
    
    
    
//CONSTANTE
    /**
     * Correspond au padding de l'objet {@link PanelAlpha}
     */
    protected final static int PADDING = 20;
    
    
    
    /**
     * Détermine si le panneau est complètement transparent ou pas
     */
    private final boolean completelyTransparent;

    
    
//CONSTRUCTORS
    /**
     * Crée un panneau {@link PanelAlpha}. Le layout est à null par défaut
     */
    public PanelAlpha() {
        this(false);
    }
    
    /**
     * Crée un panneau {@link PanelAlpha}.Le layout est à null par défaut
     * @param completelyTransparent Détermine si le panneau est complètement transparent ou pas
     */
    public PanelAlpha(boolean completelyTransparent) {
        super();
        this.completelyTransparent = completelyTransparent;
        super.setOpaque(false);
        super.setLayout(null);
    }

    
    
//METHODE PROTECTED
    /**
     * Peint l'objet {@link PanelAlpha}
     * @param g Correspond au graphique (ou toile) de l'objet à repeindre
     */
    @Override
    protected void paintComponent(java.awt.Graphics g) {
        super.paintComponent(g);
        g.setColor((completelyTransparent) ? new java.awt.Color(240, 240, 240, 0) : new java.awt.Color(240, 240, 240, 175));
        g.fillRoundRect(0, 0, getSize().width, getSize().height, 45, 45);
    }
    
    
    
//METHODES PUBLICS
    /**
     * Renvoie la largeur du panneau {@link PanelAlpha}
     * @return Retourne la largeur du panneau {@link PanelAlpha}
     */
    public final int width() {
        return getWidth();
    }
    
    /**
     * Renvoie la largeur d'un objet {@link javax.swing.JComponent}
     * @param component Correspond au {@link javax.swing.JComponent} dont on cherche à obtenir la largeur
     * @return Retourne la largeur d'un objet {@link javax.swing.JComponent}
     */
    public final int width(javax.swing.JComponent component) {
        return component.getWidth();
    }
    
    /**
     * Renvoie la largeur de l'image
     * @param image Correspond à l'image dont on cherche à obtenir la largeur
     * @return Retourne la largeur de l'image
     */
    public final int width(java.awt.Image image) {
        if(image == null) return -1;
        return image.getWidth(null);
    }
    
    /**
     * Renvoie la largeur de la dimension
     * @param dimension Correspond à la dimension dont on cherche à obtenir la largeur
     * @return Retourne la largeur de la dimension
     */
    public final int width(java.awt.Dimension dimension) {
        if(dimension == null) return -1;
        return dimension.width;
    }
    
    /**
     * Renvoie la hauteur du panneau {@link PanelAlpha}
     * @return Retourne la hauteur du panneau {@link PanelAlpha}
     */
    public final int height() {
        return getHeight();
    }
    
    /**
     * Renvoie la hauteur d'un objet {@link javax.swing.JComponent}
     * @param component Correspond au {@link javax.swing.JComponent} dont on cherche à obtenir la hauteur
     * @return Retourne la hauteur d'un objet {@link javax.swing.JComponent}
     */
    public final int height(javax.swing.JComponent component) {
        return component.getHeight();
    }
    
    /**
     * Renvoie la hauteur de l'image
     * @param image Correspond à l'image dont on cherche à obtenir la hauteur
     * @return Retourne la hauteur de l'image
     */
    public final int height(java.awt.Image image) {
        if(image == null) return -1;
        return image.getHeight(null);
    }
    
    /**
     * Renvoie la hauteur de la dimension
     * @param dimension Correspond à la dimension dont on cherche à obtenir la hauteur
     * @return Retourne la hauteur de la dimension
     */
    public final int height(java.awt.Dimension dimension) {
        if(dimension == null) return -1;
        return dimension.height;
    }
    
    /**
     * Renvoie la moitié de la largeur du panneau {@link PanelAlpha}
     * @return Retourne la moitié de la largeur du panneau {@link PanelAlpha}
     */
    public final int halfWidth() {
        return width() / 2;
    }
    
    /**
     * Renvoie la moitié de la largeur d'un objet {@link javax.swing.JComponent}
     * @param component Correspond au {@link javax.swing.JComponent} dont on cherche à obtenir la moitié de la largeur
     * @return Retourne la moitié de la largeur d'un objet {@link javax.swing.JComponent}
     */
    public final int halfWidth(javax.swing.JComponent component) {
        return width(component) / 2;
    }
    
    /**
     * Renvoie la moitié de la largeur de l'image
     * @param image Correspond à l'image dont on cherche à obtenir la moitié de la largeur
     * @return Retourne la moitié de la largeur de l'image
     */
    public final int halfWidth(java.awt.Image image) {
        return width(image) / 2;
    }
    
    /**
     * Renvoie la moitiée de la largeur de la dimension
     * @param dimension Correspond à la dimension dont on cherche à obtenir la moitiée de la largeur
     * @return Retourne la moitiée de la largeur de la dimension
     */
    public final int halfWidth(java.awt.Dimension dimension) {
        return width(dimension) / 2;
    }
    
    /**
     * Renvoie la moitié de la hauteur du panneau {@link PanelAlpha}
     * @return Retourne la moitié de la hauteur du panneau {@link PanelAlpha}
     */
    public final int halfHeight() {
        return height() / 2;
    }
    
    /**
     * Renvoie la moitié de la hauteur d'un objet {@link javax.swing.JComponent}
     * @param component Correspond au {@link javax.swing.JComponent} dont on cherche à obtenir la moitié de la hauteur
     * @return Retourne la moitié de la hauteur d'un objet {@link javax.swing.JComponent}
     */
    public final int halfHeight(javax.swing.JComponent component) {
        return height(component) / 2;
    }
    
    /**
     * Renvoie la moitié de la hauteur de l'image
     * @param image Correspond à l'image dont on cherche à obtenir la moitié de la hauteur
     * @return Retourne la moitié de la hauteur de l'image
     */
    public final int halfHeight(java.awt.Image image) {
        return height(image) / 2;
    }
    
    /**
     * Renvoie la moitiée de la hauteur de la dimension
     * @param dimension Correspond à la dimension dont on cherche à obtenir la moitiée de la hauteur
     * @return Retourne la moitiée de la hauteur de la dimension
     */
    public final int halfHeight(java.awt.Dimension dimension) {
        return height(dimension) / 2;
    }
    
    
    
}