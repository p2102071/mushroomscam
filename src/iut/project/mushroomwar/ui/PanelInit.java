/*
 * Copyright (C) 2022 IUT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package iut.project.mushroomwar.ui;



import iut.project.mushroomwar.models.Level;
import iut.project.mushroomwar.models.player.Human;
import iut.project.mushroomwar.models.player.IA;
import iut.project.mushroomwar.models.player.Player;



/**
 * Cette classe représente un {@link javax.swing.JPanel} permettant au joueur de choisir ses adversaires
 * @author Joseph BRIGUET
 * @version 1.0
 */
public final class PanelInit extends PanelAlpha {

    
    
//CONSTANTE
    /**
     * Correspond à l'espace entre le {@link #scroll} et les boutons {@link #startButton} et {@link #removeButton}
     */
    private final static int GAP_BETWEEN_COMPONENTS = 10;
    
    
    
//ATTRIBUTS
    /**
     * Correspond au tableau affichant la liste des joueurs qui vont se disputer la prochaine partie
     */
    private javax.swing.JTable datasTable;
    
    /**
     * Correspond au composant ascenseur des données de la {@link #datasTable}
     */
    private javax.swing.JScrollPane scroll;
    
    /**
     * Correspond au bouton "commencer la partie"
     */
    private javax.swing.JButton startButton;
    
    /**
     * Correspond au bouton qui permet de supprimer un joueur de la {@link #datasTable}
     */
    private javax.swing.JButton removeButton;
    
    /**
     * Correspond au bouton qui permet d'ajouter un joueur de la {@link #datasTable}
     */
    private javax.swing.JButton addButton;
    
    /**
     * Correspond au modèle qui gère la liste des joueurs qui vont se disputer la prochaine partie
     */
    private ModelTable model;
    
    /**
     * Correspond à la liste des couleurs disponibles pour les nouveaux joueurs
     */
    private java.util.List<java.awt.Color> colors;
    
    /**
     * Correspond à l'objet qui sera informé de la liste des joueurs avant de commencer la partie. Il sera également informé de son commencement
     */
    private CardInitListener listener;

    
    
//CONSTRUCTOR
    /**
     * Crée le panneau {@link PanelInit}
     */
    public PanelInit() {
        super();
        init();
    }
    
    
    
//GETTER & SETTER
    /**
     * Renvoie l'objet qui sera informé de la liste des joueurs avant de commencer la partie. Il sera également informé de son commencement
     * @return Retourne l'objet qui sera informé de la liste des joueurs avant de commencer la partie. Il sera également informé de son commencement
     */
    public final CardInitListener getInitListener(){
        return this.listener;
    }
    
    /**
     * Modifie l'objet qui sera informé de la liste des joueurs avant de commencer la partie. Il sera également informé de son commencement
     * @param listener Correspond au nouvel écouteur
     */
    public final void setInitListener(CardInitListener listener){
        this.listener = listener;
    }

    
    
//METHODE PUBLIC
    /**
     * Initialise et prépare le panneau en vue de sa future utilisation
     * @param name Correspond au nom du joueur {@link Human}
     */
    public final void init(String name) {
        this.colors = new java.util.ArrayList<>();
        this.colors.add(new java.awt.Color(  0, 127, 255));
        this.colors.add(new java.awt.Color(255,   0,   0));
        this.colors.add(new java.awt.Color(255,   0, 158));
        this.colors.add(new java.awt.Color(255,   0, 255));
        this.colors.add(new java.awt.Color(128,   0,   0));
        this.colors.add(new java.awt.Color(153,  51,   0));
        this.colors.add(new java.awt.Color(255, 102,   0));
        this.colors.add(new java.awt.Color(255, 153,   0));
        this.colors.add(new java.awt.Color(255, 204,   0));
        this.colors.add(new java.awt.Color(255, 255,   0));
        this.colors.add(new java.awt.Color(153, 204,   0));
        this.colors.add(new java.awt.Color(128, 128,   0));
        this.colors.add(new java.awt.Color(  0, 128,   0));
        this.colors.add(new java.awt.Color(  0, 204, 255));
        this.colors.add(new java.awt.Color(128,   0, 128));
        model.reset();
        model.addHuman(name);
        model.addIA();
    }

    
    
//METHODE PRIVATE
    /**
     * Initialise la vue du {@link PanelInit}
     */
    private void init() {
        java.awt.Font font = new java.awt.Font("Segoe UI", 0, 12);
        
        model = new ModelTable();
        
        datasTable   = new javax.swing.JTable(this.model);
        scroll       = new javax.swing.JScrollPane();
        startButton  = new javax.swing.JButton();
        removeButton = new javax.swing.JButton();
        addButton    = new javax.swing.JButton();

        datasTable.setShowHorizontalLines(false);
        datasTable.setShowVerticalLines(false);
        datasTable.setSelectionBackground(new java.awt.Color(200, 140, 140));
        datasTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        datasTable.getColumnModel().getColumn(0).setMinWidth(30);
        datasTable.getColumnModel().getColumn(0).setPreferredWidth(30);
        datasTable.getColumnModel().getColumn(0).setMaxWidth(30);
        datasTable.getColumnModel().getColumn(0).setResizable(false);
        datasTable.getColumnModel().getColumn(1).setResizable(false);
        datasTable.getColumnModel().getColumn(2).setResizable(false);
        datasTable.getColumnModel().getColumn(3).setResizable(false);
        datasTable.getColumnModel().getColumn(0).setCellRenderer(new CenterTextRenderer());
        datasTable.getColumnModel().getColumn(1).setCellRenderer(new CenterTextRenderer());
        datasTable.getColumnModel().getColumn(2).setCellRenderer(new ColorPlayerRenderer());
        datasTable.getColumnModel().getColumn(3).setCellRenderer(new CenterTextRenderer());
        datasTable.getColumnModel().getColumn(3).setCellEditor(new LevelCellEditor(datasTable));
        datasTable.setFont(font);
        datasTable.setRowHeight(40);

        scroll.setViewportView(datasTable);

        removeButton.setFont(font);
        removeButton.setText("Enlever");
        removeButton.addActionListener((java.awt.event.ActionEvent e) -> {
            if(model.players.size() > 1){
                int index = datasTable.getSelectedRow();
                if(index > 0)
                    model.removeIA(index);
            }
        });

        addButton.setFont(font);
        addButton.setText("Ajouter");
        addButton.addActionListener((java.awt.event.ActionEvent e) -> {
            if(model.players.size() < 15)
                model.addIA();
        });

        startButton.setFont(new java.awt.Font("Segoe UI", 0, 14));
        startButton.setText("Démarrer");
        startButton.addActionListener((java.awt.event.ActionEvent e) -> {
            if(this.listener != null)
                this.listener.evtCardInit((Human) model.players.get(0), new java.util.ArrayList<>(model.players));
        });
        
        scroll.setSize(500, 400);
        removeButton.setSize(100, 30);
        addButton.setSize(100, 30);
        startButton.setSize(130, 50);
        
        add(scroll);
        add(removeButton);
        add(addButton);
        add(startButton);
    }
    
    
    
//METHODE PROTECTED
    /**
     * Peint l'objet {@link PanelInit}
     * @param g Correspond au graphique (ou toile) de l'objet à repeindre
     */
    @Override
    protected final void paintComponent(java.awt.Graphics g) {
        super.paintComponent(g);

        scroll.setLocation(
                halfWidth()  - halfWidth(scroll), 
                halfHeight() - halfHeight(scroll) - height(addButton) - GAP_BETWEEN_COMPONENTS
        );
        
        removeButton.setLocation(
                halfWidth()  - halfWidth(scroll), 
                halfHeight() - halfHeight(scroll) - height(removeButton) + height(scroll)
        );
        
        addButton.setLocation(
                halfWidth()  - halfWidth(scroll)  + width(scroll)  - width(addButton), 
                halfHeight() - halfHeight(scroll) + height(scroll) - height(addButton)
        );
        
        startButton.setLocation(
                width()  - width(startButton)  - PADDING, 
                height() - height(startButton) - PADDING
        );
    }

    
    
//MODELE
    /**
     * Cette classe représente le modèle pour afficher la liste des joueurs (qui vont se disputer une partie) dans une {@link javax.swing.JTable}
     * @author Joseph BRIGUET
     * @version 1.0
     */
    private class ModelTable extends javax.swing.table.AbstractTableModel {

        
        
    //ATTRIBUTS
        /**
         * Correspond au nom des colonnes de la {@link javax.swing.JTable}
         */
        private final String[] columns;

        /**
         * Correspond à la liste des joueurs qui vont disputer la prochaine partie
         */
        private final java.util.List<Player> players;

        
        
    //CONSTRUCTOR
        /**
         * Crée un modèle pour la {@link javax.swing.JTable}
         */
        public ModelTable() {
            this.columns = new String[]{"", "Joueur", "Couleur", "Difficulté"};
            this.players = new java.util.ArrayList<>();
        }

        
        
    //METHODES PUBLICS
        /**
         * Ajoute un joueur {@link Human}
         * @param name Correspond au prénom de l'{@link Human}
         */
        public void addHuman(String name) {
            Human human = new Human(name, colors.remove(0));
            players.add(human);
            super.fireTableRowsInserted(0, players.size() - 1);
            refreshStateButton();
        }

        /**
         * Ajoute un joueur {@link IA}
         */
        public void addIA() {
            IA ia = new IA("IA", colors.remove(0), Level.MEDIUM);
            players.add(ia);
            super.fireTableRowsInserted(0, players.size() - 1);
            refreshStateButton();
        }
        
        /**
         * Supprime un joueur {@link IA}
         * @param row Correspond au numéro de la ligne qui représente le joueur à supprimer
         */
        public void removeIA(int row){
            Player ia = players.remove(row);
            colors.add(ia.getColor());
            super.fireTableRowsDeleted(0, players.size() - 1);
            refreshStateButton();
            datasTable.clearSelection();
        }
        
        /**
         * Renvoie la liste des joueurs qui vont se disputer la partie
         * @return Retourne la liste des joueurs qui vont se disputer la partie
         */
        public java.util.List<Player> getPlayers(){
            return new java.util.ArrayList<>(this.players);
        }
        
        /**
         * Efface tout le tableau
         */
        public void reset(){
            players.clear();
            super.fireTableRowsDeleted(0, 0);
            refreshStateButton();
        }

        /**
         * Renvoie le nom d'une colonne
         * @param column Correspond au numéro de la colonne dont on cherche le nom
         * @return Retourne le nom de la colonne
         */
        @Override
        public String getColumnName(int column) {
            return columns[column];
        }

        /**
         * Renvoie le nombre de lignes dans la {@link javax.swing.JTable}
         * @return Retourne le nombre de lignes dans la {@link javax.swing.JTable}
         */
        @Override
        public int getRowCount() {
            return players.size();
        }

        /**
         * Renvoie le nombre de colonnes dans la {@link javax.swing.JTable}
         * @return Retourne le nombre de colonnes dans la {@link javax.swing.JTable}
         */
        @Override
        public int getColumnCount() {
            return columns.length;
        }
        
        /**
         * Renvoie si oui ou non la cellule est éditable ou pas
         * @param rowIndex Correspond au numéro de la ligne de la cellule
         * @param columnIndex Correspond au numéro de la colonne de la cellule
         * @return Retourne true si la cellule est éditable, sinon false
         */
        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return rowIndex > 0 && columnIndex == 3;
        }
        
        /**
         * Lorsque le contenu d'une cellule vient d'être modifiée par l'intermédiaire d'un editeur et que la valeur doit être prise en compte au niveau de l'affichage et de l'objet concerné
         * @param value Correspond à la nouvelle valeur de la cellule. Celle-ci doit impérativement être prise en compte, sans quoi la valeur tombe dans le néant
         * @param rowIndex Correspond au numéro de la ligne de la cellule qui a été modifiée
         * @param columnIndex Correspond au numéro de la colonne de la cellule qui a été modifiée
         */
        @Override
        public void setValueAt(Object value, int rowIndex, int columnIndex) {
            Player player = (Player) players.get(rowIndex);
            if(player instanceof IA){
                IA ia = (IA)player;
                ia.setLevel((Level) value);
            }
            super.setValueAt(value, rowIndex, columnIndex);
        }

        /**
         * Renvoie la valeur d'une cellule
         * @param rowIndex Correspond au numéro de la ligne de la cellule dont on cherche à récupérer sa valeur
         * @param columnIndex Correspond au numéro de la colonne de la cellule dont on cherche à récupérer sa valeur
         * @return Retourne la valeur de la cellule
         */
        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Player player = (Player)players.get(rowIndex);
            switch (columnIndex){
                case 0: 
                    return rowIndex + 1;
                case 1:
                    return player.getFirstname();
                case 2:
                    return player.getColor();
                case 3:
                    if(player instanceof IA)
                        return ((IA) player).getLevel();
                    else
                        return null;
                default:
                    return "";
            }
        }
        
        
        
    //METHODE PRIVATE
        /**
         * Raffraichit l'état des boutons {@link #addButton} et {@link #removeButton}. Ainsi en fonction du nombre de joueurs dans la liste, ils s'activent ou se désactive
         */
        private void refreshStateButton(){
            addButton.setEnabled(players.size() < 15);
            removeButton.setEnabled(players.size() > 2);
        }
        
        

    }
    
    
    
//RENDERERS
    /**
     * Cette classe représente un objet Renderer qui a pour fonction de centrer le texte des cellules
     * @author Joseph BRIGUET
     * @version 1.0
     */
    private class CenterTextRenderer extends javax.swing.table.DefaultTableCellRenderer {
        
        

        /**
         * Renvoie le composant qui affiche la donnée de la cellule
         * @param table Correspond à la {@link javax.swing.JTable} concernée par l'affichage de la donnée d'une cellule
         * @param value Correspond à l'objet à afficher dans la cellule
         * @param isSelected Détermine si la cellule est séelctionnée ou pas
         * @param hasFocus Détermine si la cellule a le focus
         * @param row Correspond au numéro de la ligne de la cellule à afficher
         * @param column Correspond au numéro de la colonne de la cellule à afficher
         * @return Retourne le composant qui affiche la donnée de la cellule
         */
        @Override
        public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            javax.swing.JLabel label = (javax.swing.JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            label.setHorizontalAlignment(CENTER);
            return label;
        }

        
        
    }
    
    /**
     * Cette classe représente un objet Renderer qui a pour fonction d'afficher la couleur du joueur dans la cellule
     * @author Joseph BRIGUET
     * @version 1.0
     */
    private class ColorPlayerRenderer extends CenterTextRenderer {

        
        
        /**
         * Renvoie le composant qui affiche la donnée de la cellule
         * @param table Correspond à la {@link javax.swing.JTable} concernée par l'affichage de la donnée d'une cellule
         * @param value Correspond à l'objet à afficher dans la cellule
         * @param isSelected Détermine si la cellule est séelctionnée ou pas
         * @param hasFocus Détermine si la cellule a le focus
         * @param row Correspond au numéro de la ligne de la cellule à afficher
         * @param column Correspond au numéro de la colonne de la cellule à afficher
         * @return Retourne le composant qui affiche la donnée de la cellule
         */
        @Override
        public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            javax.swing.JLabel label = (javax.swing.JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            label.setBackground((java.awt.Color) value);
            label.setText("");
            return label;
        }

        
        
    }
    
    
    
//EDITOR
    /**
     * Cette classe représente un objet éditeur des cellules de niveaux de difficulté
     * @author Joseph BRIGUET
     * @version 1.0
     */
    private class LevelCellEditor  extends javax.swing.AbstractCellEditor implements javax.swing.table.TableCellEditor {

        
        
    //ATTRIBUT
        /**
         * Correspond à la liste déroulante contenant tous les niveaux de difficultés
         */
        private final javax.swing.JComboBox<Level> combo;
             
        
        
    //CONSTRUCTOR
        /**
         * Crée un éditeur de {@link Level}. Cet éditeur se présentera comme une liste déroulante contenant tous les niveaux de difficulté
         * @param table Correspond à la {@link javax.swing.JTable} concernée par cet éditeur
         */
        public LevelCellEditor(javax.swing.JTable table) {
            this.combo = new javax.swing.JComboBox<>(Level.values());
            this.combo.addActionListener((java.awt.event.ActionEvent e) -> {
                stopCellEditing();
                table.clearSelection();
            });
        }
        
        
        
    //METHODES PUBLICS
        /**
         * Renvoie la valeur de l'édition
         * @return Retourne la valeur de l'édition
         */
        @Override
        public Object getCellEditorValue() {
            return (Level) this.combo.getSelectedItem();
        }

        /**
         * Renvoie le composant qui édite la cellule
         * @param table Correspond au {@link javax.swing.JTable} dont une cellule est éditée
         * @param value Correspond à la valeur de la cellule avant l'édition
         * @param isSelected Détermine si la cellule est sélectionnée ou pas
         * @param row Correspond à la ligne de la cellule éditée
         * @param column Correspond à la colonne de la cellule éditée
         * @return Retourne le composant qui édite la cellule
         */
        @Override
        public java.awt.Component getTableCellEditorComponent(javax.swing.JTable table, Object value, boolean isSelected, int row, int column) {
            this.combo.setSelectedItem(value);
            return this.combo;
        }

        
        
    }
    
    
    
//INTERFACE
    /**
     * Cette interface permet de faire le lien entre un objet et un objet {@link PanelInit}
     * @author Joseph BRIGUET
     * @version 1.0
     */
    public interface CardInitListener {



        /**
         * Lorsque l'utilisateur a cliqué sur le bouton Jouer
         * @param human Correspond au joueur humain. Celui qui lance la partie
         * @param players Correspond à la liste des joueurs. Le joueur humain est compris dedans
         */
        public void evtCardInit(Human human, java.util.List<Player> players);



    }

    
    
}